$(document).ready( () => {

  //click event on asterisk
  $('#about').click( () => {
    //toggles
    $('.intro').toggleClass('invisible');
    $('.about').toggleClass('invisible');
    $('#about').toggleClass('underline');
    //add and remove
    $('#contact-img').addClass('invisible');
    $('#contact').removeClass('underline');
  });

  //click event on explainer overlay
  $('#contact').click( () => {
    //toggles
    $('#contact-img').toggleClass('invisible');
    $('#contact').toggleClass('underline');
    //add and remove
    $('.intro').addClass('invisible');
    $('.about').addClass('invisible');
    $('#about').removeClass('underline');
  });
});
